class IndexController < ApplicationController

	def setuppage
		@books = Book.all
		if !params[:id].blank?
			@id = params[:id]
		else
			@id = 1
		end
		if params[:marked]
			@marked = 1
		end
		if !params[:page]
			params[:page] = (@id.to_i / 40) + 1
		end
		@readverses = Verse.all.paginate(page: params[:page], per_page: 40)
	end

	def index
		@title = "Olin's Bible Concordance"
		setuppage
	end

	def search
		setuppage
		@colors = Color.all
		if !params[:input].blank?
			@input = params[:input].downcase
			@words = @input.split(" ")
			@restrictive = params[:restrictive]
			if @words
				if @restrictive == "1"
					@verses = Verse.joins(:words).where(:words => { :word => @words}).group("verses.id").having("count(verses.id) = " + @words.count.to_s)
				else
					@verses = Verse.joins(:words).where(:words => { :word => @words})
				end
				@verses_read = nil
			end
		end
		render :index
	end

	def readverse
		setuppage
		render :index
	end

	def bookinfo
		book = Book.find(params[:book_id])
		hash = []
		verses = Verse.all.where(:book_id => params[:book_id])
		for verse in verses
			if !hash[verse.chapter_number]
				hash[verse.chapter_number] = verse.id
			end
		end
		
		json = { :book => book, :verses => hash }.to_json
		render :text => json
	end
end
