class Verse < ActiveRecord::Base
	belongs_to :book
	has_many :verse_words
	has_many :words, :through => :verse_words

	def chapter_and_verse
		return self.chapter_number.to_s + ":" + self.verse_number.to_s
	end
end
