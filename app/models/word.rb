class Word < ActiveRecord::Base
	has_many :verse_words
	has_many :verses, :through => :verse_words
end
