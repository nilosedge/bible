# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ -> $("#draggable").draggable()

$ -> $('#draggableclose').click -> $('#draggable').toggle()

window.loadwindow = (id) -> 
  $("#draggable").show() if !$("#draggable").is(":visible")
  $.ajax
    url: '/index/bookinfo'
    type: 'GET'
    data:
      book_id: id
    success: (data, status, response) ->
      document.getElementById("booksummary").innerHTML = generatehtml(data)
    error: ->
      console.log "Error"
    dataType: "json"

  return true

window.generatehtml = (data) ->
  html = ""
  html += "<h3>" + data.book.name + "</h3>"
  html += "<h4>Chapters:</h4>"
  html += "<br/>"
  for chapter_number in [1..(data.verses.length - 1)]
    html += "<a href=\"/index/readverse/" + data.verses[chapter_number] + "?marked=1\">" + chapter_number + "</a> "
  return html
