# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require File.expand_path("../seeds/color_seed", __FILE__) # Requires:
require File.expand_path("../seeds/book_seed", __FILE__) # Requires:
require File.expand_path("../seeds/word_seed", __FILE__) # Requires:
require File.expand_path("../seeds/verse_seed", __FILE__) # Requires:
require File.expand_path("../seeds/verseword_seed", __FILE__) # Requires:
