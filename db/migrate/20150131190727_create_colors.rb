class CreateColors < ActiveRecord::Migration
	def change
		create_table :colors do |t|
			t.string :tag
			t.timestamps
		end
	end
end
