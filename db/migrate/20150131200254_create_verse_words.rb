class CreateVerseWords < ActiveRecord::Migration
	def change
		create_table :verse_words do |t|
			t.references :verse, index: true
			t.references :word, index: true
		end
	end
end
