class CreateBooks < ActiveRecord::Migration
	def change
		create_table :books do |t|
			t.string :name
			t.timestamps
		end
		add_index :books, :name
	end
end
