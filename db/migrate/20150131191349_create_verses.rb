class CreateVerses < ActiveRecord::Migration
	def change
		create_table :verses do |t|
			t.belongs_to :book, :null => false, index: true
			t.integer :chapter_number
			t.integer :verse_number
			t.text :verse_text
			t.timestamps
		end
		add_index :verses, :chapter_number
		add_index :verses, :verse_number
	end
end
