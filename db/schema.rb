# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150131200254) do

  create_table "books", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "books", ["name"], name: "index_books_on_name"

  create_table "colors", force: true do |t|
    t.string   "tag"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "verse_words", force: true do |t|
    t.integer "verse_id"
    t.integer "word_id"
  end

  add_index "verse_words", ["verse_id"], name: "index_verse_words_on_verse_id"
  add_index "verse_words", ["word_id"], name: "index_verse_words_on_word_id"

  create_table "verses", force: true do |t|
    t.integer  "book_id",        null: false
    t.integer  "chapter_number"
    t.integer  "verse_number"
    t.text     "verse_text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "verses", ["book_id"], name: "index_verses_on_book_id"
  add_index "verses", ["chapter_number"], name: "index_verses_on_chapter_number"
  add_index "verses", ["verse_number"], name: "index_verses_on_verse_number"

  create_table "words", force: true do |t|
    t.string   "word"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "words", ["word"], name: "index_words_on_word"

end
