Color.create({ :id => 1, :tag => "ff9999"})
Color.create({ :id => 2, :tag => "99ff99"})
Color.create({ :id => 3, :tag => "a0ffff"})
Color.create({ :id => 4, :tag => "ffff66"})
Color.create({ :id => 5, :tag => "9999ff"})
Color.create({ :id => 6, :tag => "ffa0ff"})

puts "#{Color.count} colors added"
